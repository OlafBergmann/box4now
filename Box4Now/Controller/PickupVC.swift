//
//  PickupVC.swift
//  Box4Now
//
//  Created by Olaf Bergmann on 23/12/2018.
//  Copyright © 2018 Olaf Bergmann. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import AudioToolbox

class PickupVC: UIViewController, Alertable {
    
    @IBOutlet weak var pickupMapView: RoundMapView!
    
    var regionRadius: CLLocationDistance = 2000
    
    var pin: MKPlacemark? = nil
    
    var pickupCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var userKey: String = ""
    
    var locationPlacemark: MKPlacemark!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickupMapView.delegate = self
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        locationPlacemark = MKPlacemark(coordinate: pickupCoordinate)
        dropPinFor(placemark: locationPlacemark)

        centerMapOnLocation(location: locationPlacemark.location!)
        
        DataService.instance.REF_TRIPS.child(userKey).observe(.value, with: { (tripSnapshot) in
            if tripSnapshot.exists() {
                if tripSnapshot.childSnapshot(forPath: "tripIsAccepted").value as? Bool == true {
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func initData(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D, userKey: String) {
        self.pickupCoordinate = pickupCoordinate
        self.userKey = userKey
    }

    @IBAction func cancelBtnWasPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func acceptPackageBtnWasPressed(_ sender: Any) {
        let currentUserId = Auth.auth().currentUser?.uid

        //DataService.instance.REF_TRIPS.child(userKey).value(forKey: "tripIsAccepted") as! Bool == false
        DataService.instance.REF_TRIPS.child(userKey).observeSingleEvent(of: .value, with: { (tripSnapshot) in
            let tripDict = tripSnapshot.value as? Dictionary<String, AnyObject>
            if tripDict?["tripIsAccepted"] as? Bool == false {
                if currentUserId != nil {
                    
                    UpdateService.instance.acceptTrip(withPackageKey: self.userKey, forCourierKey: currentUserId!)
                    self.presentingViewController?.shouldPresentLoadingView(true)
                    
                }
            } else {
                self.showAlert("Sorry, package is already taken")
                self.presentingViewController?.shouldPresentLoadingView(true)
            }
        })
    
    }
    
}
